<?php

namespace Drupal\window_debugger\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * An AJAX command for calling the jQuery after() method.
 *
 * @see http://docs.jquery.com/Manipulation/after#content
 *
 * @ingroup ajax
 */
class DrupalLogAjax implements CommandInterface {

  /**
   * A string that contains the debug content.
   *
   * @var string
   */
  private $data;

  /**
   * Constructs an DrupalLogAjax.
   *
   * @param string $data
   *   The string data.
   */
  public function __construct($data) {
    $this->data = (string) $data;
  }

  /**
   * Render ajax command.
   *
   * @return array|string
   *   ajax command function
   */
  public function render() {
    return [
      'command' => 'debugger',
      'message' => $this->data,
    ];
  }

}
