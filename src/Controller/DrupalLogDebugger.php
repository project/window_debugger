<?php

namespace Drupal\window_debugger\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\window_debugger\Ajax\DrupalLogAjax;
use Drupal\Core\Ajax\AjaxResponse;

/**
 * Controller routines for debuger routes.
 */
class DrupalLogDebugger extends ControllerBase {

  /**
   * {@inheritdoc}
   *
   * @var string
   */
  private $content;

  /**
   * JSON response object for AJAX requests.
   *
   * @var object
   * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21AjaxResponse.php/class/AjaxResponse/8.2.x
   */
  private $ajaxresponse;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->ajaxresponse = new AjaxResponse();
    $this->content = !empty($_SESSION['windowDebugger']) ? $_SESSION['windowDebugger'] : [];
  }

  /**
   * Add an AJAX command to the response.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The core ajax response.
   */
  public function get() {
    if (empty($this->content)) {
      return $this->ajaxresponse->addCommand(new DrupalLogAjax("<div class='item'><span class='empty'>not record</span><br/></div>"));
    }
    if(\is_array($this->content)) $this->content = implode('', $this->content);
    $this->ajaxresponse->addCommand(new DrupalLogAjax($this->content));
    return $this->ajaxresponse;
  }

  /**
   * {@inheritdoc}
   */
  public static function remove() {
    $_SESSION['windowDebugger'] = [];
    $response = new \Symfony\Component\HttpFoundation\Response();
    return $response->setContent('');
  }

  /**
   * {@inheritdoc}
   */
  public function __destruct() {
    $this->content = [];
  }

}
