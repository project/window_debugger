<?php

namespace Drupal\window_debugger\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures window_debugger settings.
 */
class DrupalLogSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'window_debugger_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['window_debugger.settings'];
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'window_debugger/window_debugger.mousetrap';
    $form['#attached']['library'][] = 'window_debugger/window_debugger.record';
    $form['#attached']['library'][] = 'window_debugger/window_debugger.hotkey';

    $form['debugger'] = [
      '#type'           => 'checkbox',
      '#title'          => $this->t('Enable @moduleName', ['@moduleName' => '"Window Debugger"']),
      '#default_value'  => $this->config('window_debugger.settings')->get('debugger'),
      '#return_value'   => 1,
      '#id'             => 'edit-debugger',
    ];

    $form['debugger_for_all'] = [
      '#type'           => 'checkbox',
      '#title'          => $this->t('Enable @moduleName for all users', ['@moduleName' => '"Window Debugger"']),
      '#default_value'  => $this->config('window_debugger.settings')->get('debugger_for_all'),
      '#states'   => [
        'invisible' => [
          '#edit-debugger' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['debugger_hotkey'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Configure hotkeys'),
      '#default_value'  => $this->config('window_debugger.settings')->get('debugger_hotkey'),
      '#id'             => 'window_debugger__hotkey',
      '#attributes'     => [
        'autocomplete'    => 'off'
      ]
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('window_debugger.settings')->set('debugger', $form_state->getValue('debugger'))->save();
    $this->config('window_debugger.settings')->set('debugger_for_all', $form_state->getValue('debugger_for_all'))->save();
    $this->config('window_debugger.settings')->set('debugger_hotkey', $form_state->getValue('debugger_hotkey'))->save();
    return parent::submitForm($form, $form_state);
  }

}
