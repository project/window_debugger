/**
 * @file
 * {@inheritdoc}
 */

'use strict'

let gulp  = require('gulp'),
  htmlmin = require('gulp-htmlmin'),
  inlinesource = require('gulp-inline-source');


// Gulp task to minify HTML files
// adn include js, css file
gulp.task('build', function () {
  var options = {
    compress: true
  };

  return gulp.src('./misc/app/index.html')
    .pipe(inlinesource(options))
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./misc/dist'));
});
