/**
 * DO NOT EDIT THIS FILE.
 * @file
 */
;
(function ($, Drupal,drupalSettings) {

  /**
   * Add debugger command.
   */
  Drupal.AjaxCommands.prototype.debugger = function (ajax, response, status) {
    var debuggerWin = window.open('', 'window_debugger', 'status=no,location=no,toolbar=no,menubar=no,width=800,height=800,left=' + (window.innerWidth - 20));

    /**
     * page layout request
     * @return string
     */
    if (!$(debuggerWin.document).find('#debugerContainer').length) {
      $.ajax({
        url: '/' + drupalSettings.debuggerWindowHTML,
        success: function (html) {
          debuggerWin.document.write(html);
        },
        async: false
      });
    }
    debuggerWin.document.write(response.message);
    debuggerWin.focus();
  };

  /**
   * Calling a debugging page
   */
  Mousetrap.bind(drupalSettings.debuggerHotkey, function() {
    Drupal.ajax({
      url: '/debug/get',
      submit: {},
      element: document.getElementsByTagName('body')[0],
      progress: {
        type: 'throbber'
      }
    }).execute();
  });

})(jQuery, Drupal,drupalSettings);
