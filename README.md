CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Required modules
 * Installation
 * Configuration
 * Using

INTRODUCTION
------------

 Helps retrieve change information, such as in places where the kint cannot be
 displayed  using once debug tool.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/window_debugger

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/window_debugger

REQUIRED MODULES
-------------------

 * Require devel kint: https://www.drupal.org/project/devel

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

CONFIGURATION
-------------

 * To use, you must enable debugging on the page:
   https://YOUR_SITE.NAME/admin/config/window_debugger/settings


USING
-------------

```php
wd($data = NULL, int $maxLevels = NULL);
```

Where is:
 * **$data** - Your variable
 * **$maxLevels** -  @var int max array/object levels to go deep, if zero no
 limits are applied

After ajax request (reloading page, sending webform), press:
# CTRL + SHIFT + ENTER (key code: 17, 16,13)
