var admin_clear__first  = document.getElementById('admin_clear__first');
var admin_clear__last   = document.getElementById('admin_clear__last');
var admin_clear__all    = document.getElementById('admin_clear__all');
var admin_clear__data    = document.getElementById('admin_clear__data');

admin_clear__all.addEventListener('click', function (e) {
  let listDebuger = document.querySelectorAll('.item');
  if (!listDebuger.length) return false;
  for (let i = 0; i < listDebuger.length; i++) {
    listDebuger[i].parentNode.removeChild(listDebuger[i]);
  }
});

admin_clear__last.addEventListener('click', function (e) {
  let listDebuger = document.getElementsByClassName('item');
  if (!listDebuger.length) return false;
  listDebuger[listDebuger.length - 1].parentNode.removeChild(listDebuger[listDebuger.length - 1]);
});

admin_clear__first.addEventListener('click', function (e) {
  let listDebuger = document.getElementsByClassName('item');
  if (!listDebuger.length) return false;
  listDebuger[0].parentNode.removeChild(listDebuger[0]);
});

admin_clear__data.addEventListener('click', function (){
  admin_clear__all.click();
  var xhr = new XMLHttpRequest();
  console.log(window.location.origin + '/debug/remove');
  xhr.open("POST", window.location.origin + '/debug/remove', true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
  xhr.send();
});
